import Config from "../../config";
import IMarinaProvider from "../../interfaces/iMarinaProvider";
import MarinaNodeNamoProvider from "./nodenamo/marinaNodeNamoProvider";

export default class MarinaProviderFactory {
    public static create(): IMarinaProvider {
        switch(Config.DATA_PROVIDER) {
            case Config.LOCAL_DYNAMO_DB_PROVIDER:
                return new MarinaNodeNamoProvider({
                    region: Config.AWS_DEFAULT_REGION,
                    endpoint: Config.LOCAL_DYNAMO_DB_ENDPOINT
                })
            default:
                throw new Error(`Unsupported provider '${Config.DATA_PROVIDER}', please use one of the following providers: [${Config.DATA_PROVIDERS_LIST.join(", ")}]`)
        }
    }
}