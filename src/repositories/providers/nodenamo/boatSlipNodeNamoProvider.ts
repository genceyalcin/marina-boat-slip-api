import { NodeNamo } from "nodenamo";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { NodeNamoBoatSlip } from "./nodeNamoBoatSlip";
import IBoatSlipProvider from "../../../interfaces/iBoatSlipProvider";
import IBoatSlip from "../../../interfaces/iBoatSlip";

export default class BoatSlipNodeNamoProvider implements IBoatSlipProvider
{
    private nodenamoClient: NodeNamo;

    constructor(options?: object) {
        this.nodenamoClient = new NodeNamo(new DocumentClient(options));
    }

    async listMarinaBoatSlips(marinaId: string): Promise<IBoatSlip[]> {
        const keyParams = {
            keyConditions: "#hash = :hash",
            expressionAttributeValues: {
                ":hash": marinaId
            },
            expressionAttributeNames: {
                "#hash": "hash"
            }
        }
        let lastEvaluatedKey: string;
        let slips: IBoatSlip[] = []; 
        do {
            const thisPage = await this.nodenamoClient.find()
                .from(NodeNamoBoatSlip).where(keyParams).execute<IBoatSlip>();
            slips.push(...thisPage.items);
            lastEvaluatedKey = thisPage.lastEvaluatedKey;
        }
        while(lastEvaluatedKey);

        return slips;        
    }

    async getOne(id: string): Promise<IBoatSlip> {
        return await this.nodenamoClient.get(id).from(NodeNamoBoatSlip).execute<IBoatSlip>();
    }

    async create(boatSlip: IBoatSlip): Promise<void> {
        await this.nodenamoClient.insert(boatSlip).into(NodeNamoBoatSlip).execute();
    }

    async update(boatSlip: IBoatSlip): Promise<void> {
        await this.nodenamoClient.update(boatSlip).from(NodeNamoBoatSlip).execute();
    }

    async delete(id: string): Promise<void> {
        await this.nodenamoClient.delete(id).from(NodeNamoBoatSlip).execute();
    }
}