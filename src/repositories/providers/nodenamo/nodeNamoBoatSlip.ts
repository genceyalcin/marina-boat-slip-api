import { DBColumn, DBTable } from "nodenamo";
import Config from "../../../config";
import IBoatSlip from "../../../interfaces/iBoatSlip";

@DBTable({name: Config.BOAT_SLIP_DYNAMO_DB_TABLE_NAME})
export class NodeNamoBoatSlip implements IBoatSlip {
    @DBColumn({id: true})
    id: string;

    @DBColumn({hash: true})
    marinaId: string;

    @DBColumn({range: true})
    slipNumber: number;

    @DBColumn()
    vacant: boolean;

    @DBColumn()
    vesselName: string;
}