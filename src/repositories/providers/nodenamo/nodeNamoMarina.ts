import { DBColumn, DBTable } from "nodenamo";
import Config from "../../../config";
import IMarina from "../../../interfaces/iMarina";

@DBTable({name: Config.MARINA_DYNAMO_DB_TABLE_NAME})
export default class NodeNamoMarina implements IMarina {
    @DBColumn({id: true})
    id: string;

    @DBColumn()
    capacity: number;

    @DBColumn()
    vacancies: number;

    @DBColumn()
    vacantSlips: number[];
}