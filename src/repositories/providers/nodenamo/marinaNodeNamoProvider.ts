import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { NodeNamo } from "nodenamo";
import IMarina from "../../../interfaces/iMarina";
import IMarinaProvider from "../../../interfaces/iMarinaProvider";
import NodeNamoMarina from "./nodeNamoMarina";

export default class MarinaNodeNamoProvider implements IMarinaProvider
{
    private nodenamoClient: NodeNamo;

    constructor(options?: object) {
        this.nodenamoClient = new NodeNamo(new DocumentClient(options));
    }

    async create(marina: IMarina): Promise<void> {
        await this.nodenamoClient.insert(marina).into(NodeNamoMarina).execute();
    }

    async update(marina: IMarina): Promise<void> {
        await this.nodenamoClient.update(marina).from(NodeNamoMarina).execute();
    }

    async getOne(id: string): Promise<IMarina> {
        return await this.nodenamoClient.get(id).from(NodeNamoMarina).execute<IMarina>();
    }
}