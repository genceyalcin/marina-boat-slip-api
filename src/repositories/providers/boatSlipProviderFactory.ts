import Config from "../../config";
import IBoatSlipProvider from "../../interfaces/iBoatSlipProvider";
import BoatSlipNodeNamoProvider from "./nodenamo/boatSlipNodeNamoProvider";

export default class BoatSlipProviderFactory {
    public static create(): IBoatSlipProvider {
        switch(Config.DATA_PROVIDER) {
            case Config.LOCAL_DYNAMO_DB_PROVIDER:
                return new BoatSlipNodeNamoProvider({
                    region: Config.AWS_DEFAULT_REGION,
                    endpoint: Config.LOCAL_DYNAMO_DB_ENDPOINT
                })
            default:
                throw new Error(`Unsupported provider '${Config.DATA_PROVIDER}', please use one of the following providers: [${Config.DATA_PROVIDERS_LIST.join(", ")}]`)
        }
    }
}