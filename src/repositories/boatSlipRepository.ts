import IBoatSlip from "../interfaces/iBoatSlip";
import IBoatSlipProvider from "../interfaces/iBoatSlipProvider";
import IBoatSlipRepository from "../interfaces/iBoatSlipRepository";
import BoatSlipProviderFactory from "./providers/boatSlipProviderFactory";

export default class BoatSlipRepository implements IBoatSlipRepository
{
    constructor(private provider: IBoatSlipProvider = BoatSlipProviderFactory.create()) {}

    async listMarinaBoatSlips(marinaId: string): Promise<IBoatSlip[]> {
        return await this.provider.listMarinaBoatSlips(marinaId);
    }

    async getOne(id: string): Promise<IBoatSlip> {
        return await this.provider.getOne(id);
    }
    
    async delete(id: string): Promise<void> {
        await this.provider.delete(id);
    }

    async update(boatSlip: IBoatSlip): Promise<void> {
        await this.provider.update(boatSlip);
    }

    async create(boatSlip: IBoatSlip): Promise<void> {
        await this.provider.create(boatSlip);
    } 
}