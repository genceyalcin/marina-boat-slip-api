import IMarina from "../interfaces/iMarina";
import IMarinaProvider from "../interfaces/iMarinaProvider";
import IMarinaRepository from "../interfaces/iMarinaRepository";
import MarinaProviderFactory from "./providers/marinaProviderFactory";

export default class MarinaRepository implements IMarinaRepository
{
    constructor(private provider: IMarinaProvider = MarinaProviderFactory.create()) {}

    async update(marina: IMarina): Promise<void> {
        await this.provider.update(marina);
    }

    async getOne(id: string): Promise<IMarina> {
        return await this.provider.getOne(id);
    }
}