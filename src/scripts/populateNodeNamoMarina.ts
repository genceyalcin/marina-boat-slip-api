import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { createCommand } from "commander";
import { NodeNamo } from "nodenamo";
import BoatSlipNodeNamoProvider from "../repositories/providers/nodenamo/boatSlipNodeNamoProvider";
import Config from "../config";
import MarinaNodeNamoProvider from "../repositories/providers/nodenamo/marinaNodeNamoProvider";
import { NodeNamoBoatSlip } from "../repositories/providers/nodenamo/nodeNamoBoatSlip";
import NodeNamoMarina from "../repositories/providers/nodenamo/nodeNamoMarina";
import Marina from "../entities/marina";
import BoatSlip from "../entities/boatSlip";

async function main() {
    let command = createCommand("initialize-marina")
        .requiredOption("-c, --capacity <number>", "Capacity of the marina to create")
        .requiredOption("-e, --env <string>", "Environment to run script on")
        .parse(process.argv);
    
    const capacity: number = Number(command.opts()['capacity']);
    const environment: string = command.opts()['env'];

    let options;
    if (environment === "local") {
        options = {
            endpoint: Config.LOCAL_DYNAMO_DB_ENDPOINT,
            region: Config.AWS_DEFAULT_REGION
        }
    }
    else {
        throw new Error("Unsupported environment! Please use one of the following: [local]");
    }    

    const nodeNamoClient = new NodeNamo(new DocumentClient(options)); 
    const boatSlipProvider = new BoatSlipNodeNamoProvider(options);
    const marinaProvider = new MarinaNodeNamoProvider(options);

    console.log("Creating tables:");
    await tryToCreateTable(NodeNamoBoatSlip, nodeNamoClient);
    await tryToCreateTable(NodeNamoMarina, nodeNamoClient);
    console.log("  Created tables!");

    console.log("Creating marina...");
    const newMarina = new Marina({
        id: Config.THIS_MARINA_ID,
        capacity: capacity,
        vacancies: capacity,
        vacantSlips: Array(capacity).fill(0).map((v, i) => i+1)
    })
    await marinaProvider.create(newMarina);
    console.log("  Created marina!");

    console.log("Creating boat slips...")
    for(let i = 0; i < capacity; i++) {
        let boatSlipToCreate = new BoatSlip({
            id: `${Config.THIS_MARINA_ID}#${i+1}`,
            marinaId: Config.THIS_MARINA_ID,
            slipNumber: i+1,
            vacant: true
        })
        await boatSlipProvider.create(boatSlipToCreate);
    }
    console.log(" Created boat slips!")
}

async function tryToCreateTable(NodeNamoTable: any, nodeNamoClient: NodeNamo) {
    try {
        await nodeNamoClient.createTable().for(NodeNamoTable).execute();
    }
    catch(e) {
        await purgeTable(NodeNamoTable, nodeNamoClient);
    }
}

async function purgeTable(NodeNamoTable: any, nodeNamoClient: NodeNamo) {
    console.log(`  Table already exists... Purging table.`)
    let lastEvaluatedKey: string;
    let itemsToDelete = [];
    do {
        let thisPage = await nodeNamoClient.list().from(NodeNamoTable).execute();
        itemsToDelete.push(...thisPage.items);
        lastEvaluatedKey = thisPage.lastEvaluatedKey;
    }
    while (lastEvaluatedKey);
    for (let item of itemsToDelete) {
        await nodeNamoClient.delete(item.id).from(NodeNamoTable).execute();
    }
}

main().catch((e) => {
    console.log(e);
    console.log("Something went wrong, please run this script again...");
})
.then(() => console.log("Done!"));