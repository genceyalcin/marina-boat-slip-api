export default interface IMarina {
    id: string;
    capacity: number;
    vacancies: number;
    vacantSlips: number[];
}