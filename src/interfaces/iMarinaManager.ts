import IMarina from "./iMarina";

export default interface IMarinaManager {
    update(marina: IMarina): Promise<void>;
    getOne(id?: string): Promise<IMarina>;
}