import IMarina from "./iMarina";

export default interface IMarinaRepository {
    update(marina: IMarina): Promise<void>;
    getOne(id: string): Promise<IMarina>;
}