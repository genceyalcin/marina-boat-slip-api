import IBoatSlip from "./iBoatSlip";

export default interface IBoatSlipManager {
    listMarinaBoatSlips(marinaId?: string): Promise<IBoatSlip[]>;
    getOne(slipNumber: number, marinaId?: string): Promise<IBoatSlip>
    occupy(vesselName: string, slipNumber: number, marinaId?: string): Promise<void>;
    vacate(slipNumber: number, marinaId?: string): Promise<void>;
}