import IBoatSlip from "./iBoatSlip";

export default interface IBoatSlipProvider {
    listMarinaBoatSlips(marinaId: string): Promise<IBoatSlip[]>;
    getOne(id: string): Promise<IBoatSlip>
    delete(id: string): Promise<void>
    create(boatSlip: IBoatSlip): Promise<void>;
    update(boatSlip: IBoatSlip): Promise<void>;
}