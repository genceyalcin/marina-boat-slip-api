export default interface IListBoatSlipsView {
    slipNumber: number;
    vacant: boolean;
    vesselName: string;
}