export default interface IBoatSlip {
    id: string;
    marinaId: string;
    slipNumber: number;
    vacant: boolean;
    vesselName: string;
}