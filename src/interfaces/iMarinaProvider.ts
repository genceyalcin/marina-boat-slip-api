import IMarina from "./iMarina";

export default interface IMarinaProvider {
    create(marina: IMarina): Promise<void>;
    update(marina: IMarina): Promise<void>;
    getOne(id: string): Promise<IMarina>;
}