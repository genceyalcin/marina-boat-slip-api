import IBoatSlip from "./iBoatSlip";

export default interface IBoatSlipRepository {
    listMarinaBoatSlips(marinaId: string): Promise<IBoatSlip[]>;
    getOne(id: string): Promise<IBoatSlip>
    update(boatSlip: IBoatSlip): Promise<void>;
    create(boatSlip: IBoatSlip): Promise<void>;
    delete(id: string): Promise<void>
}