import { IEvent } from "configurapi";
import { JsonResponse } from "configurapi-handler-json";
import BoatSlipManager from "../managers/boatSlipManager";
import MarinaManager from "../managers/marinaManager";

export async function vacateBoatSlipHandler(event: IEvent,
    marinaManager = new MarinaManager(), boatSlipManager = new BoatSlipManager()) 
{
    const slipNumber = event.params['boat-slip'];
    let boatSlip = await boatSlipManager.getOne(slipNumber);
    if (boatSlip === undefined) {
        event.response = new JsonResponse({
            statusCode: 404,
            message: `Boat slip '${slipNumber}' does not exist in this marina!`
        }, 404);
        return this.complete();
    }

    if (!boatSlip.vacant) {
        let thisMarina = await marinaManager.getOne();
        thisMarina.vacancies++;
        thisMarina.vacantSlips.push(slipNumber);
        
        await boatSlipManager.vacate(slipNumber);
        await marinaManager.update(thisMarina);

        event.response.statusCode = 204;
        return this.continue();
    }
    else {
        event.response = new JsonResponse({
            statusCode: 409,
            message: `Boat slip '${slipNumber}' is currently vacant`
        }, 409);
        return this.complete();
    }
}