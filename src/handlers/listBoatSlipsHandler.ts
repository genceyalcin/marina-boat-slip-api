import { IEvent } from "configurapi";
import { JsonResponse } from "configurapi-handler-json";
import IBoatSlipManager from "../interfaces/iBoatSlipManager";
import BoatSlipManager from "../managers/boatSlipManager";
import ListBoatSlipsView from "../views/listBoatSlipsView";

export async function listBoatSlipsHandler(event: IEvent, 
    manager: IBoatSlipManager = new BoatSlipManager()) 
{
    const boatSlips = await manager.listMarinaBoatSlips();
    event.response = new JsonResponse(boatSlips.map(bs => new ListBoatSlipsView(bs)));
    return this.continue();
}