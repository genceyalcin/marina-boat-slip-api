import { IEvent } from "configurapi";
import { JsonResponse } from "configurapi-handler-json";
import IBoatSlipManager from "../interfaces/iBoatSlipManager";
import IMarinaManager from "../interfaces/iMarinaManager";
import BoatSlipManager from "../managers/boatSlipManager";
import MarinaManager from "../managers/marinaManager";

export async function occupyFirstAvailableBoatSlipHandler(event: IEvent, 
    marinaManager: IMarinaManager = new MarinaManager(), boatSlipManager: IBoatSlipManager = new BoatSlipManager()) 
{
    const vesselName = event.payload.vesselName;
    let thisMarina = await marinaManager.getOne();
    if (thisMarina.vacancies > 0) {
        const slipToOccupy = thisMarina.vacantSlips.pop();
        thisMarina.vacancies--;
        
        await boatSlipManager.occupy(vesselName, slipToOccupy);
        await marinaManager.update(thisMarina);
        event.response = new JsonResponse({slipNumber: slipToOccupy}, 200);
        
        return this.continue();
    }
    else {
        event.response = new JsonResponse({
            statusCode: 409,
            message: "There are no available boat slips."
        }, 409);
        return this.complete();
    }
}