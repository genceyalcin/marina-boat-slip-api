export default class Config {
    public static readonly THIS_MARINA_ID = process.env.THIS_MARINA_ID || "this-marina-id";

    // Provider Config
    public static readonly LOCAL_DYNAMO_DB_PROVIDER = process.env.LOCAL_DYNAMO_DB_PROVIDER || "local-ddb";
    public static readonly DATA_PROVIDERS_LIST = [Config.LOCAL_DYNAMO_DB_PROVIDER];
    public static readonly DATA_PROVIDER = process.env.DATA_PROVIDER || Config.LOCAL_DYNAMO_DB_PROVIDER;
    
    // AWS Config
    public static readonly AWS_DEFAULT_REGION = process.env.AWS_DEFAULT_REGION || "us-east-1";
    public static readonly LOCAL_DYNAMO_DB_ENDPOINT = process.env.LOCAL_DYNAMO_DB_ENDPOINT || "http://localhost:8000/"
    public static readonly MARINA_DYNAMO_DB_TABLE_NAME = process.env.MARINA_DYNAMO_DB_TABLE_NAME || "marina-ddb-table";
    public static readonly BOAT_SLIP_DYNAMO_DB_TABLE_NAME = process.env.BOAT_SLIP_DYNAMO_DB_TABLE_NAME || "boat-slip-ddb-table";
}