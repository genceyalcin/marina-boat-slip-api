import Config from "../config";
import IMarina from "../interfaces/iMarina";
import IMarinaRepository from "../interfaces/iMarinaRepository";
import MarinaRepository from "../repositories/marinaRepository";

export default class MarinaManager {
    constructor(private repository: IMarinaRepository = new MarinaRepository()) {}
    
    async getOne(id = Config.THIS_MARINA_ID): Promise<IMarina> {
        return await this.repository.getOne(id);
    }

    async update(marina: IMarina): Promise<void> {
        return await this.repository.update(marina);
    }
}