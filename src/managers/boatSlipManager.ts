import Config from "../config";
import BoatSlip from "../entities/boatSlip";
import IBoatSlip from "../interfaces/iBoatSlip";
import IBoatSlipRepository from "../interfaces/iBoatSlipRepository";
import BoatSlipRepository from "../repositories/boatSlipRepository";

export default class BoatSlipManager {
    constructor(private repository: IBoatSlipRepository = new BoatSlipRepository()) {}

    async listMarinaBoatSlips(marinaId = Config.THIS_MARINA_ID): Promise<IBoatSlip[]> {
        return await this.repository.listMarinaBoatSlips(marinaId);
    }

    async getOne(slipNumber: number, marinaId = Config.THIS_MARINA_ID): Promise<IBoatSlip> {
        const id = BoatSlip.constructId(marinaId, slipNumber);
        return await this.repository.getOne(id);
    }  

    async occupy(vesselName: string, slipNumber: number, marinaId = Config.THIS_MARINA_ID): Promise<void> 
    {
        await this.repository.update(new BoatSlip({
            id: BoatSlip.constructId(marinaId, slipNumber),
            vacant: false,
            marinaId,
            slipNumber,
            vesselName
        }));
    }

    async vacate(slipNumber: number, marinaId = Config.THIS_MARINA_ID): Promise<void> {
        await this.repository.update(new BoatSlip({
            id: BoatSlip.constructId(marinaId, slipNumber),
            vacant: true,
            marinaId,
            slipNumber
        }));
    }
}