import IBoatSlip from "../interfaces/iBoatSlip";

export default class ListBoatSlipsView {
    slipNumber: number;
    vacant: boolean;
    vesselName: string;

    constructor(boatSlip: IBoatSlip) {
        this.slipNumber = boatSlip.slipNumber;
        this.vacant = boatSlip.vacant;
        this.vesselName = boatSlip.vacant ? undefined : boatSlip.vesselName;
    }
}