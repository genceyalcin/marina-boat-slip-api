import IBoatSlip from "../interfaces/iBoatSlip";

export default class BoatSlip implements IBoatSlip {
    id: string;
    marinaId: string;
    slipNumber: number;
    vacant: boolean;
    vesselName: string;

    constructor(slip: Partial<IBoatSlip>) {
        this.id = slip?.id;
        this.marinaId = slip?.marinaId;
        this.slipNumber = slip?.slipNumber;
        this.vesselName = slip?.vesselName;
        this.vacant = slip?.vacant;
    }

    public static fromJson(obj: object) {
        return obj ? new BoatSlip(obj) : undefined;
    }

    public static constructId(marinaId: string, slipNumber: number) {
        return `${marinaId}#${slipNumber}`;
    }
}