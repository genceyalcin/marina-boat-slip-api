import IMarina from "../interfaces/iMarina";

export default class Marina implements IMarina {
    id: string;
    capacity: number;
    vacancies: number;
    vacantSlips: number[];

    constructor(marina: Partial<IMarina>) {
        this.id = marina?.id;
        this.capacity = marina?.capacity;
        this.vacancies = marina?.vacancies;
        this.vacantSlips = marina?.vacantSlips;
    }

    public static fromJson(obj: object) {
        return obj ? new Marina(obj) : undefined;
    }
}