import IBoatSlipManager from "../src/interfaces/iBoatSlipManager";
import IMarinaManager from "../src/interfaces/iMarinaManager";
import IMarina from "../src/interfaces/iMarina";
import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import { IEvent } from "configurapi";
import { Context } from "./helper";
import { occupyFirstAvailableBoatSlipHandler } from "../src/handlers/occupyFirstAvailableBoatSlipHandler";

describe("occupyFirstAvailableBoatSlipHandler", () => {
    let mockedSlipManager: IMock<IBoatSlipManager>;
    let mockedMarinaManager: IMock<IMarinaManager>;
    let marinaStateUpdated: boolean;
    let boatSlipUpdated: boolean;
    let event: IEvent;
    let context: Context;
    let currentMarina: IMarina;

    beforeEach(() => {
        mockedSlipManager = Mock.ofType<IBoatSlipManager>();
        mockedMarinaManager = Mock.ofType<IMarinaManager>();
        event = <IEvent>{
            payload: {
                vesselName: "skipper"
            }
        };
        marinaStateUpdated = false;
        boatSlipUpdated = false;
        context = new Context();
        currentMarina = {
            id: "marina-id",
            capacity: 3,
            vacancies: 3,
            vacantSlips: [1, 2, 3]
        }
        mockedMarinaManager
            .setup(m => m.getOne())
            .returns(async() => currentMarina);
    });

    it("Occupy, vacant slip exists", async() => {
        const expectedUpdatedMarina: IMarina = {
            id: "marina-id",
            capacity: 3,
            vacancies: 2,
            vacantSlips: [1, 2]
        }
        mockedMarinaManager
            .setup(m => m.update(It.isValue(expectedUpdatedMarina)))
            .callback(() => marinaStateUpdated = true);
        
        mockedSlipManager
            .setup(m => m.occupy(It.isValue("skipper"), It.isValue(3)))
            .callback(() => boatSlipUpdated = true);

        await occupyFirstAvailableBoatSlipHandler.apply(context, [event, 
            mockedMarinaManager.object, mockedSlipManager.object]);
        
        const expectedResponse = {
            slipNumber: 3
        };

        assert.isTrue(marinaStateUpdated);
        assert.isTrue(boatSlipUpdated);
        assert.isTrue(context.continued);
        assert.deepEqual(event.response.body, expectedResponse);
        assert.deepEqual(event.response.statusCode, 200);   
    })

    it ("Occupy, vacant slip does not exist", async() => {
        currentMarina.vacancies = 0;
        currentMarina.vacantSlips = [];

        await occupyFirstAvailableBoatSlipHandler.apply(context, [event, 
            mockedMarinaManager.object, mockedSlipManager.object]);

        const expectedResponse = {
            statusCode: 409,
            message: "There are no available boat slips."
        };

        assert.isTrue(context.completed);
        assert.deepEqual(event.response.body, expectedResponse);
        assert.deepEqual(event.response.statusCode, 409);  
    })
})