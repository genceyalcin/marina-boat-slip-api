import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import BoatSlip from "../src/entities/boatSlip";
import IBoatSlipManager from "../src/interfaces/iBoatSlipManager";
import IBoatSlipRepository from "../src/interfaces/iBoatSlipRepository";
import BoatSlipManager from "../src/managers/boatSlipManager";

describe("BoatSlipManager", () => {
    let mockedRepository: IMock<IBoatSlipRepository>;
    let manager: IBoatSlipManager;
    let repositoryCalled: boolean;

    beforeEach(() => {
        mockedRepository = Mock.ofType<IBoatSlipRepository>();
        manager = new BoatSlipManager(mockedRepository.object);
        repositoryCalled = false;
    })

    it('occupy', async () => {
        const expectedBoatSlip = new BoatSlip({
            id: "marina-id#1",
            marinaId: "marina-id",
            slipNumber: 1,
            vacant: false,
            vesselName: "vessel-name"
        })
        mockedRepository
            .setup(r => r.update(It.isValue(expectedBoatSlip)))
            .callback(() => repositoryCalled = true);

        await manager.occupy("vessel-name", 1, "marina-id");
        assert.isTrue(repositoryCalled);
    })
    
    it('vacate', async () => {
        const expectedBoatSlip = new BoatSlip({
            id: "marina-id#1",
            marinaId: "marina-id",
            slipNumber: 1,
            vacant: true
        });
        mockedRepository
            .setup(r => r.update(It.isValue(expectedBoatSlip)))
            .callback(() => repositoryCalled = true);

        await manager.vacate(1, "marina-id");
        assert.isTrue(repositoryCalled);
    })

    const passThroughCases = [
        { function: "listMarinaBoatSlips", argument: "marina-id" }
    ]
    passThroughCases.forEach((c) => {
        it(c.function, async() => {
            mockedRepository
                .setup(r => r[c.function](It.isValue(c.argument)))
                .callback(() => repositoryCalled = true);
            
            manager[c.function](c.argument);
            assert.isTrue(repositoryCalled);
        })
    })
})