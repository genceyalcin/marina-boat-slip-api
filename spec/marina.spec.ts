import Marina from "../src/entities/marina";
import { assert } from "chai"

describe("Marina business entity", () => {
    it("Constructor", () => {
        const marina = new Marina({
            id: "marina-id",
            capacity: 3,
            vacancies: 1,
            vacantSlips: [2]
        })

        assert.strictEqual(marina.id, "marina-id");
        assert.strictEqual(marina.capacity, 3);
        assert.strictEqual(marina.vacancies, 1);
        assert.deepEqual(marina.vacantSlips, [2]);
    })

    it("From JSON", () => {
        const marina = Marina.fromJson({
            id: "marina-id",
            capacity: 3,
            vacancies: 1,
            vacantSlips: [2]
        })

        assert.strictEqual(marina.id, "marina-id");
        assert.strictEqual(marina.capacity, 3);
        assert.strictEqual(marina.vacancies, 1);
        assert.deepEqual(marina.vacantSlips, [2]);
    })
})