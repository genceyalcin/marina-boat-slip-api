import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import Marina from "../src/entities/marina";
import IMarinaManager from "../src/interfaces/iMarinaManager";
import IMarinaRepository from "../src/interfaces/iMarinaRepository";
import MarinaManager from "../src/managers/marinaManager";

describe("MrinaManager", () => {
    let mockedRepository: IMock<IMarinaRepository>;
    let manager: IMarinaManager;
    let repositoryCalled: boolean;

    beforeEach(() => {
        mockedRepository = Mock.ofType<IMarinaRepository>();
        manager = new MarinaManager(mockedRepository.object);
        repositoryCalled = false;
    })

    const passThroughCases = [
        { function: "update", argument: "marina-id" },
        { function: "getOne", argument: "marina-id" }
    ]
    passThroughCases.forEach((c) => {
        it(c.function, async() => {
            mockedRepository
                .setup(r => r[c.function](It.isValue(c.argument)))
                .callback(() => repositoryCalled = true);
            
            manager[c.function](c.argument);
            assert.isTrue(repositoryCalled);
        })
    })
})