import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import Marina from "../src/entities/marina";
import IMarinaProvider from "../src/interfaces/iMarinaProvider";
import IMarinaRepository from "../src/interfaces/iMarinaRepository";
import MarinaRepository from "../src/repositories/marinaRepository";

describe("MarinaRepository", () => {
    let mockedProvider: IMock<IMarinaProvider>;
    let repository: IMarinaRepository;
    let providerCalled: boolean;

    beforeEach(() => {
        mockedProvider = Mock.ofType<IMarinaProvider>();
        repository = new MarinaRepository(mockedProvider.object);
        providerCalled = false;
    })

    const passThroughCases = [
        { function: "getOne", argument: "marina-id" },
        { function: "update", argument: new Marina({id:"marina-id"})}
    ]
    passThroughCases.forEach((c) => {
        it(c.function, async() => {
            mockedProvider
                .setup(p => p[c.function](It.isValue(c.argument)))
                .callback(() => providerCalled = true);
            
            repository[c.function](c.argument);
            assert.isTrue(providerCalled);
        })
    })
})