import BoatSlip from "../src/entities/boatSlip";
import { assert } from "chai"

describe("BoatSlip business entity", () => {
    it("Constructor", () => {
        const boatSlip = new BoatSlip({
            id: "marina-id#3",
            slipNumber: 3,
            vacant: false,
            vesselName: "vessel-name"
        })

        assert.strictEqual(boatSlip.id, "marina-id#3");
        assert.strictEqual(boatSlip.slipNumber, 3);
        assert.strictEqual(boatSlip.vacant, false);
        assert.strictEqual(boatSlip.vesselName, "vessel-name");
    })

    it("From JSON", () => {
        const boatSlip = BoatSlip.fromJson({
            id: "marina-id#3",
            slipNumber: 3,
            vacant: false,
            vesselName: "vessel-name"
        })

        assert.strictEqual(boatSlip.id, "marina-id#3");
        assert.strictEqual(boatSlip.slipNumber, 3);
        assert.strictEqual(boatSlip.vacant, false);
        assert.strictEqual(boatSlip.vesselName, "vessel-name");
    })
})