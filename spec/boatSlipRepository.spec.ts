import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import BoatSlip from "../src/entities/boatSlip";
import IBoatSlipProvider from "../src/interfaces/iBoatSlipProvider";
import IBoatSlipRepository from "../src/interfaces/iBoatSlipRepository";
import BoatSlipRepository from "../src/repositories/boatSlipRepository";

describe("BoatSlipRepository", () => {
    let mockedProvider: IMock<IBoatSlipProvider>;
    let repository: IBoatSlipRepository;
    let providerCalled: boolean;

    beforeEach(() => {
        mockedProvider = Mock.ofType<IBoatSlipProvider>();
        repository = new BoatSlipRepository(mockedProvider.object);
        providerCalled = false;
    })

    const passThroughCases = [
        { function: "listMarinaBoatSlips", argument: "marina-id" },
        { function: "getOne", argument: "marina-id" },
        { function: "update", argument: new BoatSlip({id:"marina-id#1"})},
        { function: "delete", argument: "marina-id" },
        { function: "create", argument: new BoatSlip({id:"marina-id#1"})}
    ]
    passThroughCases.forEach((c) => {
        it(c.function, async() => {
            mockedProvider
                .setup(p => p[c.function](It.isValue(c.argument)))
                .callback(() => providerCalled = true);
            
            repository[c.function](c.argument);
            assert.isTrue(providerCalled);
        })
    })
})