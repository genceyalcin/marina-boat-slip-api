import IBoatSlipManager from "../src/interfaces/iBoatSlipManager";
import BoatSlipManager from "../src/managers/boatSlipManager";
import IBoatSlip from "../src/interfaces/iBoatSlip";
import IMarinaManager from "../src/interfaces/iMarinaManager";
import IMarina from "../src/interfaces/iMarina";
import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import { IEvent } from "configurapi";
import { Context } from "./helper";
import { vacateBoatSlipHandler } from "../src/handlers/vacateBoatSlipHandler";

describe("vacateBoatSlipHandler", () => {
    let mockedSlipManager: IMock<IBoatSlipManager>;
    let mockedMarinaManager: IMock<IMarinaManager>;
    let marinaStateUpdated: boolean;
    let boatSlipUpdated: boolean;
    let event: IEvent;
    let context: Context;
    let boatSlipToVacate: IBoatSlip;
    let currentMarina: IMarina;

    beforeEach(() => {
        mockedSlipManager = Mock.ofType<IBoatSlipManager>();
        mockedMarinaManager = Mock.ofType<IMarinaManager>();
        event = <IEvent>{
            params: {},
            response: {}
        };
        event.params['boat-slip'] = 1;
        boatSlipToVacate = {
            id: "marina-id#1",
            marinaId: "marina-id",
            slipNumber: 1,
            vacant: false,
            vesselName: "skipper"
        }
        currentMarina = {
            id: "marina-id",
            capacity: 3,
            vacancies: 2,
            vacantSlips: [2, 3]
        }
        marinaStateUpdated = false;
        boatSlipUpdated = false;
        context = new Context();
        mockedSlipManager
            .setup(m => m.getOne(It.isValue(1)))
            .returns(async() => boatSlipToVacate);
        mockedMarinaManager
            .setup(m => m.getOne())
            .returns(async() => currentMarina);
    });

    it("Vacate, occupied slip", async() => {
        const expectedUpdatedMarina: IMarina = {
            id: "marina-id",
            capacity: 3,
            vacancies: 3,
            vacantSlips: [2, 3, 1]
        }
        mockedMarinaManager
            .setup(m => m.update(It.isValue(expectedUpdatedMarina)))
            .callback(() => marinaStateUpdated = true);

        mockedSlipManager
            .setup(m => m.vacate(It.isValue(1)))
            .callback(() => boatSlipUpdated = true);

        await vacateBoatSlipHandler.apply(context, [event, 
            mockedMarinaManager.object, mockedSlipManager.object]);
        
        assert.isTrue(context.continued);
        assert.isTrue(boatSlipUpdated);
        assert.isTrue(marinaStateUpdated);
        assert.strictEqual(event.response.statusCode, 204);
    })

    it("Vacate, already vacant slip", async() => {
        boatSlipToVacate.vacant = true;

        await vacateBoatSlipHandler.apply(context, [event, 
            mockedMarinaManager.object, mockedSlipManager.object]);
            
        const expectedResponseBody = {
            statusCode: 409,
            message: "Boat slip '1' is currently vacant"
        }
        assert.isTrue(context.completed);
        assert.deepEqual(event.response.body, expectedResponseBody)
        assert.strictEqual(event.response.statusCode, 409);
    })

    it("Vacate, invalid slip number", async() => {
        event.params['boat-slip'] = 6;

        await vacateBoatSlipHandler.apply(context, [event, 
            mockedMarinaManager.object, mockedSlipManager.object]);
            
        const expectedResponseBody = {
            statusCode: 404,
            message: "Boat slip '6' does not exist in this marina!"
        }
        assert.isTrue(context.completed);
        assert.deepEqual(event.response.body, expectedResponseBody)
        assert.strictEqual(event.response.statusCode, 404);
    })
})