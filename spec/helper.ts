export class Context {
    continued: boolean;
    completed: boolean;

    constructor() {
        this.continued = true;
        this.completed = true;
    }

    continue() {
        this.continued = true;
    }

    complete() {
        this.completed = true;
    }
}