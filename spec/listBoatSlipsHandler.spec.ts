import IBoatSlipManager from "../src/interfaces/iBoatSlipManager";
import { assert } from "chai";
import { IMock, It, Mock } from "typemoq";
import { listBoatSlipsHandler } from "../src/handlers/listBoatSlipsHandler";
import { IEvent } from "configurapi";
import IBoatSlip from "../src/interfaces/iBoatSlip";
import { Context } from "./helper";

describe("listBoatSlipsHandler", () => {
    let mockedManager: IMock<IBoatSlipManager>;
    let managerCalled: boolean;
    let event: IEvent;
    let context: Context;

    beforeEach(() => {
        mockedManager = Mock.ofType<IBoatSlipManager>();
        event = <IEvent>{};
        managerCalled = false;
        context = new Context();
    });

    it("List slips", async() => {
        const slipsToReturn: IBoatSlip[] = [
            {
                id: "marina-id#1",
                slipNumber: 1,
                marinaId: "marina-id",
                vacant: false,
                vesselName: "vessel-name"
            }
        ]
        mockedManager
            .setup(m => m.listMarinaBoatSlips())
            .callback(() => managerCalled = true)
            .returns(async() => slipsToReturn);

        await listBoatSlipsHandler.apply(context, [event, mockedManager.object]);
        const expectedResponse = [
            {
                slipNumber: 1,
                vacant: false,
                vesselName: "vessel-name"
            }
        ]
        assert.isTrue(managerCalled);
        assert.isTrue(context.continued);
        assert.deepEqual(event.response.body, expectedResponse);
        assert.deepEqual(event.response.statusCode, 200);
    })
})