import ListBoatSlipsView from "../src/views/listBoatSlipsView";
import BoatSlip from "../src/entities/boatSlip";
import { assert } from "chai"

describe("ListBoatSlipsView", () => {
    it("Constructor, not vacant", () => {
        const view = new ListBoatSlipsView(new BoatSlip({
            id: "marina-id#1",
            slipNumber: 1,
            marinaId: "marina-id",
            vacant: false,
            vesselName: "vessel-name"
        }));

        assert.deepEqual(view, {
            slipNumber: 1,
            vacant: false,
            vesselName: "vessel-name" 
        })
    })

    it("Constructor, not vacant", () => {
        const view = new ListBoatSlipsView(new BoatSlip({
            id: "marina-id#1",
            slipNumber: 1,
            marinaId: "marina-id",
            vacant: true,
            vesselName: "vessel-name"
        }));

        assert.deepEqual(view, {
            slipNumber: 1,
            vacant: true,
            vesselName: undefined
        });
    })
})