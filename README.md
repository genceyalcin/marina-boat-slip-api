# Marina Boat Slip API

## External Dependencies
This project requires that you run a local version of DynamoDB before starting up the API. Please use these docker commands to pull and run the necessary docker container
```
docker pull amazon/dynamodb-local:latest
docker run --rm -d -p 8000:8000/tcp amazon/dynamodb-local:latest
```
then run 
```
npm populate-marina
```
to initialize the database with the vacant boat slips.

OR

you can use the first time start script provided which will do all of the above, install NPM dependencies, and run the api:
```
npm run first-start
```

After, running either of the above, you can use
```
npm start
```
for consequent executions, provided that the docker container for local DynamoDB is running

## Test
For unit testing, run
```
npm run test
```

For component testing, make sure the API and all dependencies are running, then run
```
npm run c-test
```

## Design Rationale
### Layers
The API was divided into layers to separate the business logic from the data specific logic. Additionally, it uses a provider agnostic approach so that we can switch data providers if necessary, even across different environments. 

All of the business logic is contained within the handler and manager layers, while the database specific functions are within the repository and provider levels. Every business entity that is maintained by the API has a manager, repository, and provider objects. 

In the case of this API the business entity we maintain are boat slips. There is also a hidden business entity which represents the state of the marina, described in more detail below.

### Occupation Strategy
Looking at the operations that the API must support, I thought it would be best to maintain a stack of vacant spots in the marina. This is why I created a hidden (not ever seen by the client) business entity that represents the state of the marina. When we are attempting to take a vacant boat slip, we could list all the boat slips and find the first vacant one, but this is efficient. Instead, we just pop the top slip from the stack of vacant slips, and occupy that slip. When we vacate a slip, that slip gets pushed back to the top of that stack. This way both vacating and occupying a slip has a complexity of order O(1) and allows for faster and less overall reads from the database.

### Scalability
Furthermore, the marina and boat slip entities are connected by id's. A boat slip id is the combination of it's hash and range keys which are the marina ID and the slip number respectively e.g. `this-marina-id#3`. This allows for a boat slip to be indexed immediately. This also means that, if we ever wanted to, we could expose functions to the client side that could allow us to create multiple marinas with a variable amount of boat slips that can all be operated on with the same efficiency and complexity. 

For the current solution, only one marina ID is maintained which is a value from the `Config` file. All of these configuration values can be changed accross environments using the appropriate variables.

### Limitations
Since the challenge was timeboxed, there were a some things that I wish that I could have implemented that I did not have time for:
- Revertable Chained Commands:
 - Since there are two separate entities that represent each other in state, I would have liked to implement chained commands to ensure synchronization between those states. 
